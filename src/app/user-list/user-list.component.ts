import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users = null;

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.authService.getAll()
    .subscribe(users => this.users = users);   
  }

  deleteUser(id: string) {   
    this.authService.delete(id)      
        .subscribe(() => this.users = this.users);
        window.location.reload();
}
}
