export class User {
    id: string;    
    firstName: string;
    lastName: string;
    email:string;
    role:string;
    status:string;
    username: string;
    password: string;
}