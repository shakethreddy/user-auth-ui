import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { first } from 'rxjs/operators';
import { User } from '../models/user';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted=false;
  loading:false;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password:['',Validators.required]
      
  });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.authService.login(this.f.username.value, this.f.password.value)   
      .subscribe({
        next: () => {
          const tempuser:User=JSON.parse(localStorage.getItem('user'));
          if(tempuser.role=='Admin'){            
            this.router.navigateByUrl('/users');
            localStorage.setItem('isAdmin','Admin');
          }else{
            this.router.navigateByUrl('/home');
          }
        }
    });
}

}
