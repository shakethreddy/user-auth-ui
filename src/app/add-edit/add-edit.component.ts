import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.css']
})
export class AddEditComponent implements OnInit {

  form: FormGroup;
  id: string;
  isAddMode: boolean;
  loading = false;
  submitted = false;
  rolesList: any = ['Auditor', 'Agent', 'Customer','Adjuster']
  constructor(private formBuilder: FormBuilder,private route: ActivatedRoute,
              private router: Router,private authService:AuthService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;

    // password not required in edit mode
    const passwordValidators = [Validators.minLength(6)];
    if (this.isAddMode) {
        passwordValidators.push(Validators.required);
    }

    this.form = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', passwordValidators],
      email: ['', [Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      role: ['', Validators.required],
      status: ['', Validators.required]
  });

  if (!this.isAddMode) {
    this.authService.getById(this.id)        
        .subscribe(x => this.form.patchValue(x));
  }
  }

  changRole(e) {
    console.log(e.target.value);
  }

   // convenience getter for easy access to form fields
   get f() { return this.form.controls; }

   onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;      
    }

    this.loading = true;
        if (this.isAddMode) {
            this.createUser();
        } else {
            this.updateUser();
        }
  }

  private createUser() {
    this.authService.register(this.form.value)       
        .subscribe({
            next: () => {                
                this.router.navigate(['/users'], { relativeTo: this.route });
            },
            error: error => {
               console.log('error occured while creating user :'+error );
               this.loading = false;
            }
        });
  }

  private updateUser() {
    this.authService.update(this.id, this.form.value)        
        .subscribe({
            next: () => {               
                this.router.navigate(['/users']);
            },
            error: error => {
              console.log('error occured while creating user :'+error );
              this.loading = false;
            }
        });
}
}
