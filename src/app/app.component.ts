import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'user-auth';
  user: User;
  isAdmin:boolean=false;

  constructor(private authService: AuthService) {
    this.authService.user.subscribe(x => this.user = x);
    
  }
  ngOnInit(){
    if(localStorage.getItem('isAdmin')){
      this.isAdmin=true;
    }
  }
 logout() {
  this.authService.logout();
}

}
